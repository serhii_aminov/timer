/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#define TIMEDELAYBUTTON 900
#define SYSCLK_FREQ 16000000
#define PCS 500
#define VOLUME 60

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim14;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC_Init(void);
static void MX_TIM14_Init(void);
static void MX_TIM1_Init(void);
/* USER CODE BEGIN PFP */
extern uint16_t test;
extern uint16_t testMin;
extern uint16_t testSec;
extern uint32_t number1;
extern uint32_t number2;
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void SendMin(uint16_t _Min);
void SendSec(uint16_t _Sec);
void TimeCount(void);
void TimeSleep(void);
void TimeSleepCheck(uint32_t _timeSleep);
void DisplayOff(uint8_t _SetReset);
void WakeUp(void);
void WriteBuzzer(uint16_t _freq);
void Countdown(void);
void Neutralization(uint8_t _OffIsOne);

///HAL_GPIO_EXTI_Callback///
uint32_t Jitter = 0;
uint16_t Sec = 0;
uint16_t Min = 0;
uint8_t timeStartCount = 0;
uint32_t Count = 0;
///HAL_GPIO_EXTI_Callback///

//uint32_t VOLUME = 10;

uint32_t TimeSleepOff = 0;
uint8_t StatusWork = 1;

uint8_t StateButtonStart = 0;
uint32_t TimeStateButtonStart = 0;

uint8_t StateButtonSec = 0;
uint32_t TimeStateButtonSec = 0;
uint16_t ValueSound = 0;
uint16_t StateValueSound = 0;
uint16_t ProcentValueSound = 0;

uint32_t ValueFreq = 0;
uint32_t StateValueFreq = 0;

///Countdown///
uint32_t StartSound = 0; //time when need start sound
uint8_t switchStartAndStopSound = 0; //switch delay on and off sound
uint32_t TimeDelayForBuzzerOn = 0;
uint32_t NumberDelayBuzzerOff = 0;
uint32_t TimeExplosion = 0;
//uint32_t SoundDelay_45_Sek[95] = 	{1000,900,800,800, 900, 900, 800, 800,770,750,725,700,700,675,650,625,625,625,600,600,575,550,550,525,525,500,500,475,475,475,450,450,425,425,400,400,375,375,375,350,350,350,325,300,300,300,300,275,275,275,275,250,250,250,250,200,200,200,200,200,175,175,175,175,175,150,150,150,150,150,150,150,125,125,100,100,100,100,100,75,75,75,75,75,75,75,75,50,50,50,50,50,50,50,50};		
uint32_t SoundDelay_20_Sek[62] = {475,400,400,375,375,375,350,350,350,325,300,300,300,300,275,275,275,275,250,250,250,250,200,200,200,200,200,175,175,175,175,175,150,150,150,150,150,150,150,125,125,100,100,100,100,100,75,75,75,75,75,75,75,75,50,50,50,50,50,50,50,50};		
///Countdown///

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC_Init();
  MX_TIM14_Init();
  MX_TIM1_Init();
  /* USER CODE BEGIN 2 */
	HAL_TIM_Base_Start_IT (&htim14);
	TimeSleepCheck(10000);
	DisplayOff(1);
	HAL_TIM_PWM_Start_IT(&htim1, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start_IT(&htim1, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		
		if(HAL_GetTick() > 4294967280){
		NVIC_SystemReset();
		}
		if (TimeStateButtonStart < HAL_GetTick() && StateButtonStart == 1){
			StateButtonStart = 0;
		}
		if (TimeStateButtonSec < HAL_GetTick() && StateButtonSec == 1){
			StateButtonSec = 0;
		}	

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		Countdown(); //start sound
		TimeSleep();
		if(timeStartCount == 1){
			TimeCount();
		}

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL4;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{

  /* USER CODE BEGIN ADC_Init 0 */

  /* USER CODE END ADC_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC_Init 2 */
	
  /* USER CODE END ADC_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 159;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 499;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM14 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM14_Init(void)
{

  /* USER CODE BEGIN TIM14_Init 0 */

  /* USER CODE END TIM14_Init 0 */

  /* USER CODE BEGIN TIM14_Init 1 */

  /* USER CODE END TIM14_Init 1 */
  htim14.Instance = TIM14;
  htim14.Init.Prescaler = 99;
  htim14.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim14.Init.Period = 10;
  htim14.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim14.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM14_Init 2 */

  /* USER CODE END TIM14_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0|GPIO_PIN_1, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);

  /*Configure GPIO pins : PF0 PF1 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : PA0 PA1 PA2 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA3 PA5 PA6 PA7 */
  GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PB1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_1_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);

  HAL_NVIC_SetPriority(EXTI2_3_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(EXTI2_3_IRQn);

}

/* USER CODE BEGIN 4 */
void SendMin(uint16_t _Min){
	if(_Min > 99){ 
		_Min = 0; 
		Min = 0; 
	}
	testMin = _Min;
}

void SendSec(uint16_t _Sec){
	if(_Sec > 59){
		_Sec = 0; 
		Sec = 0;
		testSec = _Sec;
	}
	testSec = _Sec; 
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	
	if(timeStartCount == 1 && StartSound == 1){ 
		Neutralization(1);//when time explosion and pressed another button we go out and stoped program
	}
	
 	if(GPIO_Pin == GPIO_PIN_2){	//send minute
			WakeUp();
			if(Jitter < HAL_GetTick()){
				if(timeStartCount == 0){
					Jitter = HAL_GetTick() + 200;
					Min++;
					SendMin(Min);
					TimeSleepCheck(10000);
				}
			}
	}
	
	if(GPIO_Pin == GPIO_PIN_1){//send second
		WakeUp();
		if(Jitter < HAL_GetTick()){
			if(timeStartCount == 0){
				Jitter = HAL_GetTick() + 200;
				Sec++;
				if(Min == 0 && Sec < 20){
					Sec = 21;
				}
				SendSec(Sec);
				TimeSleepCheck(10000);
				TimeStateButtonSec = HAL_GetTick() + TIMEDELAYBUTTON;
				StateButtonSec = 1;
			}
		}
	}
	
	if(GPIO_Pin == GPIO_PIN_0){//start count
		WakeUp();
		if(Jitter < HAL_GetTick() && timeStartCount == 0){
			Jitter = HAL_GetTick() + 300;
			TimeSleepCheck(10000);
			if( Min > 0 || Sec > 0){
				timeStartCount = 1;	
			}
			StateButtonStart = 1;
			TimeStateButtonStart = HAL_GetTick() + TIMEDELAYBUTTON;
		}
		if(Jitter < HAL_GetTick()){
			Jitter = HAL_GetTick() + 300;
			if(timeStartCount == 1 && timeStartCount == 1){
				timeStartCount = 0;
			}
			StateButtonStart = 1;
			TimeStateButtonStart = HAL_GetTick() + TIMEDELAYBUTTON;
		}	
	}	
}
void TimeCount(void){
	
	if(Sec <= 0 && Min <= 0){
		timeStartCount = 0;	
	}
	
	if(Count < HAL_GetTick() ){
		Count = HAL_GetTick() + 1000; // cout time, normal -> 1000 
		Sec = Sec - 1;
		SendSec(Sec);
		TimeSleepCheck(10000);
		SendMin(Min);		
		if( Min > 0 && Sec == 0){
			SendSec(Sec);
			SendMin(Min);
			Min = Min - 1;
			Sec = 60;	
//			SendMin(Min);
	  }			
			if(Sec == 19 && Min <= 0){
				StartSound = 1;
			}
			if(Sec == 0 && Min <= 0){
				StartSound = 1;
			}	
	}		
}

void TimeSleep(void){
	if(TimeSleepOff < HAL_GetTick()){
	//	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);
		DisplayOff(0);
	}
}

void TimeSleepCheck(uint32_t _timeSleep){
	TimeSleepOff = HAL_GetTick() + _timeSleep;
}

void DisplayOff(uint8_t _SetReset){
	
	if(_SetReset == 1){HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0, GPIO_PIN_SET); //start programm
									 	 HAL_GPIO_WritePin(GPIOF, GPIO_PIN_1, GPIO_PIN_SET);
										 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET);
										 HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
										 HAL_TIM_Base_Start_IT (&htim14);
	}
	
	if(_SetReset == 0){HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0, GPIO_PIN_RESET);//stop and off programm
									 	 HAL_GPIO_WritePin(GPIOF, GPIO_PIN_1, GPIO_PIN_RESET);
										 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);
										 HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
										 HAL_TIM_Base_Stop_IT (&htim14);
										 StatusWork = 0;
										 Sec = 0;
										 Min = 0;
										 testMin = 0;
										 testSec = 0;
	                   number1 = 0;
										 number2 = 0;
										 SendMin(0);
										 SendSec(0);
										 timeStartCount = 0;
										 NumberDelayBuzzerOff = 0;
										 HAL_SuspendTick();
										 HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);
										 switchStartAndStopSound = 0; //switch delay on and off sound
										 TimeDelayForBuzzerOn = 0;
										 NumberDelayBuzzerOff = 0;
										 TimeExplosion = 0;		
											
	}
	if(_SetReset == 2){HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0, GPIO_PIN_SET);//on display
									 	 HAL_GPIO_WritePin(GPIOF, GPIO_PIN_1, GPIO_PIN_SET);
										 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET);
										 HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
	}
	if(_SetReset == 3){HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0, GPIO_PIN_RESET);//off display
									 	 HAL_GPIO_WritePin(GPIOF, GPIO_PIN_1, GPIO_PIN_RESET);
										 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);
										 HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	}
}

void WakeUp(void){
		if(StatusWork == 0){
			SystemClock_Config();
			DisplayOff(1);
			StatusWork = 1;
			TIM14->PSC = 99;
	//		Sec = 0;
	//		Min = 0;
	//		NVIC_SystemReset();
	//		HAL_ResumeTick();
	}
}
void WriteBuzzer(uint16_t _freq){
		ProcentValueSound = VOLUME;
		if(_freq == 0){
			TIM1->CCR3 = 0;
			TIM1->CCR2 = 0;
//			HAL_TIM_PWM_Stop_IT(&htim1, TIM_CHANNEL_3);
//	    HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_3);
		} else {
//			HAL_TIM_PWM_Stop_IT(&htim1, TIM_CHANNEL_3);
//	    HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_3);
			StateValueSound = 0;
		}
		if(ProcentValueSound != StateValueSound){			
			ValueSound = ProcentValueSound * (PCS / 100);
			TIM1->CCR2 = ValueSound - 1;
			TIM1->CCR3 = ValueSound - 1;
			StateValueSound = ProcentValueSound;
		}
		ValueFreq = _freq;
		if(ValueFreq != StateValueFreq && _freq != 0){
			TIM1->PSC = (SYSCLK_FREQ  / (PCS * ValueFreq)) - 1; //prescaller
			StateValueFreq = ValueFreq;
		}
}
void Countdown(void){
	if(StartSound == 1 /*&& timeStartCount == 1*/){ //time when need start sound
		if(NumberDelayBuzzerOff == 62 && timeStartCount == 0){
			NumberDelayBuzzerOff = 63;
			WriteBuzzer(2400);
			TimeExplosion = HAL_GetTick() + 3000;
		}
		if (switchStartAndStopSound == NumberDelayBuzzerOff && TimeDelayForBuzzerOn < HAL_GetTick() && TimeExplosion == 0){ //switch delay on and off sound, if swithc 0 write on sound
			WriteBuzzer(2400);
			TimeDelayForBuzzerOn = HAL_GetTick() + 125;
			NumberDelayBuzzerOff++;		
		}	
		if (NumberDelayBuzzerOff > switchStartAndStopSound && TimeDelayForBuzzerOn < HAL_GetTick() && TimeDelayForBuzzerOn < HAL_GetTick() && TimeExplosion == 0){ //switch delay on and off sound, if swithc 1 write off sound
			TimeDelayForBuzzerOn = HAL_GetTick() + SoundDelay_20_Sek[NumberDelayBuzzerOff];
			WriteBuzzer(0);
			switchStartAndStopSound = NumberDelayBuzzerOff;
		}		
		if(NumberDelayBuzzerOff == 63 && TimeExplosion < HAL_GetTick()){
			StartSound = 0;
			WriteBuzzer(0);
			NumberDelayBuzzerOff = 0;
			TimeDelayForBuzzerOn = 0;
			TimeExplosion = 0;
			switchStartAndStopSound = 0;
		}
	}
	if(StartSound == 1 && timeStartCount == 0 && NumberDelayBuzzerOff < 63){
		WriteBuzzer(0);
	}
}
void Neutralization(uint8_t _OffIsOne){
	if(_OffIsOne == 1){
		timeStartCount = 2;
		StartSound = 0;
		WriteBuzzer(0);
		TimeSleepCheck(10000);
		TIM14->PSC = 20000;

	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
