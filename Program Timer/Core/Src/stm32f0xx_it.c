/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f0xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint16_t testMin = 0;
uint16_t testSec = 0;
uint32_t number1 = 0;
uint32_t number2 = 0;
uint16_t test1 = 0;
uint16_t test2 = 0;
uint16_t test3 = 0;
uint16_t test4 = 0;
uint16_t test5 = 0;
uint16_t test6 = 0;

uint8_t cnt = 0;
uint16_t test = 0;
uint8_t shift = 0;
uint8_t data[10] = {0xFC, 0x60, 0xDA, 0xF2, 0x66, 0xB6, 0xBE, 0xE0, 0xFE, 0xF6};
uint8_t data_Point[10] = {0xFD, 0x61, 0xDB, 0xF3, 0x67, 0xB7, 0xBF, 0xE1, 0xFF, 0xF7};
uint8_t data_Point_Off[10] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t name = 0;
uint32_t number = 0;
uint16_t dig1 = 0;
uint16_t dig2 = 0;
uint16_t dig3 = 0;
uint16_t dig4 = 0;
uint16_t  dataSPI = 0;
uint32_t chset = 0;
uint32_t point = 0;
uint32_t chsetsdviga = 0;
extern int16_t enc;
uint32_t start = 0;
uint32_t kol_impulsov = 10;
uint32_t a = 0;
uint32_t b = 0;
uint32_t Cnt1 = 0;
uint32_t us = 0;

uint16_t TestTim = 0;
/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim14;
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M0 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */
  while (1)
  {
  }
  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVC_IRQn 0 */

  /* USER CODE END SVC_IRQn 0 */
  /* USER CODE BEGIN SVC_IRQn 1 */

  /* USER CODE END SVC_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F0xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f0xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles EXTI line 0 and 1 interrupts.
  */
void EXTI0_1_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI0_1_IRQn 0 */

  /* USER CODE END EXTI0_1_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
  /* USER CODE BEGIN EXTI0_1_IRQn 1 */

  /* USER CODE END EXTI0_1_IRQn 1 */
}

/**
  * @brief This function handles EXTI line 2 and 3 interrupts.
  */
void EXTI2_3_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI2_3_IRQn 0 */

  /* USER CODE END EXTI2_3_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
  /* USER CODE BEGIN EXTI2_3_IRQn 1 */

  /* USER CODE END EXTI2_3_IRQn 1 */
}

/**
  * @brief This function handles TIM1 break, update, trigger and commutation interrupts.
  */
void TIM1_BRK_UP_TRG_COM_IRQHandler(void)
{
  /* USER CODE BEGIN TIM1_BRK_UP_TRG_COM_IRQn 0 */

  /* USER CODE END TIM1_BRK_UP_TRG_COM_IRQn 0 */
  HAL_TIM_IRQHandler(&htim1);
  /* USER CODE BEGIN TIM1_BRK_UP_TRG_COM_IRQn 1 */

  /* USER CODE END TIM1_BRK_UP_TRG_COM_IRQn 1 */
}

/**
  * @brief This function handles TIM1 capture compare interrupt.
  */
void TIM1_CC_IRQHandler(void)
{
  /* USER CODE BEGIN TIM1_CC_IRQn 0 */
	TestTim ++;
  /* USER CODE END TIM1_CC_IRQn 0 */
  HAL_TIM_IRQHandler(&htim1);
  /* USER CODE BEGIN TIM1_CC_IRQn 1 */

  /* USER CODE END TIM1_CC_IRQn 1 */
}

/**
  * @brief This function handles TIM14 global interrupt.
  */
void TIM14_IRQHandler(void)
{
  /* USER CODE BEGIN TIM14_IRQn 0 */
//    number = test;
	  number1 = testMin;
  	number2 = testSec;
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET); //STCP
	
//	dig1 = number/1000; // 1
//	dig4 = number%1000; //234
//	dig2 = dig4/100; // 2
//	dig4 = dig4%100;// 34
//	dig3 = dig4/10; // 3
//	dig4 = dig4%10; // 4
	
		test4 = testMin / 10;
		test5 = testMin%10;
		test6 = test5 / 1;	
	
		test1 = testSec / 10;
		test2 = testSec%10;
		test3 = test2 / 1;
		
		dig3 = test1;
		dig4 = test3;
		
		dig1 = test4;
		dig2 = test6;
	
		if (chset==0){point=dig1;}
	  if (chset==1){point=dig2;}
	  if (chset==2){point=dig3;}
	  if (chset==3){point=dig4;}
	
	cnt++;
  chsetsdviga++;
	
	if(chset == 1){
		
		if(cnt <= 16)
	{
	
	if(cnt & (1<<0))
	{
	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET); //SHCP
	}
	else
  {
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET); //SHCP
	}
	
	 name = cnt/2; 
	
	if(cnt <= 16)
	{ 
		shift =  1<<(name); 
      
      if(data_Point[point] & shift)	
			{
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);//DS
			}
      else
			{
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET); //DS
			}			
		}		
	}	
	
	if(cnt == 16)
	{
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_SET); //STCP
		
	if(chsetsdviga==16){HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0, GPIO_PIN_SET);} //NUMB1
	else {HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0, GPIO_PIN_RESET);}	
	if(chsetsdviga==32){HAL_GPIO_WritePin(GPIOF, GPIO_PIN_1, GPIO_PIN_SET);} //NUMB2
	else {HAL_GPIO_WritePin(GPIOF, GPIO_PIN_1, GPIO_PIN_RESET);}		
	if(chsetsdviga==48){HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET);} //NUMB3
	else {HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);}		
	if(chsetsdviga==64){HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);} //NUMB4
	else {HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);}			
		
		chset++;
		cnt=0;
//	  if (chsetsdviga==64){}	
		if (chsetsdviga==64){chsetsdviga=0;}
		if (chset==4){chset=0;}		
	}
	else{HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET);} //STCP
	
	} else {
	
	if(cnt <= 16)
	{
	
	if(cnt & (1<<0))
	{
	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET); //SHCP
	}
	else
  {
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET); //SHCP
	}
	
	 name = cnt/2; 
	
	if(cnt <= 16)
	{ 
		shift =  1<<(name); 
      
      if(data[point] & shift)	
			{
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);//DS
			}
      else
			{
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET); //DS
			}			
		}		
	}	
	
	if(cnt == 16)
	{
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_SET); //STCP
		
	if(chsetsdviga==16){HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0, GPIO_PIN_SET);} //NUMB1
	else {HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0, GPIO_PIN_RESET);}	
	if(chsetsdviga==32){HAL_GPIO_WritePin(GPIOF, GPIO_PIN_1, GPIO_PIN_SET);} //NUMB2
	else {HAL_GPIO_WritePin(GPIOF, GPIO_PIN_1, GPIO_PIN_RESET);}		
	if(chsetsdviga==48){HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET);} //NUMB3
	else {HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);}		
	if(chsetsdviga==64){HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);} //NUMB4
	else {HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);}			
		
		chset++;
		cnt=0;
//	  if (chsetsdviga==64){}	
		if (chsetsdviga==64){chsetsdviga=0;}
		if (chset==4){chset=0;}		
	}
	else{HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET);} //STCP
}
	
  /* USER CODE END TIM14_IRQn 0 */
  HAL_TIM_IRQHandler(&htim14);
  /* USER CODE BEGIN TIM14_IRQn 1 */

  /* USER CODE END TIM14_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
